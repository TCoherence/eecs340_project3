from simulator.node import Node
import json
from copy import deepcopy

# Here is the description of our data structure
# forward_table: str -> str(ID and SEQ) -> (ID):list
# {
#               ------------------------ -> showed in the following code
#   '0': {'0': [MIN_DIST, [PATH]], 'SEQ': 1}
#   '1': {}
#   ...
#   'k': {}
# }

# Index of the list in dictionary
MIN_DIST = 0
PATH = 1
INT_MAX = 2**20 - 1

class Distance_Vector_Node(Node):
    ###### user defined data
    # From parent Class Node /simulator/node.py
    # self.id = id
    # self.neighbors = []
    # self.logging = logging.getLogger('Node %d' % self.id)
    

    def __init__(self, id):
        # self.id = id
        # self.neighbors = []
        # self.logging = logging.getLogger('Node %d' % self.id)
        super().__init__(id)
        # seq # 
        self.seq = 0
        self.forward_table = {
            str(self.id): {
                str(self.id): [0, []],
                'SEQ': 0
            }
        } # we use '-1' to present the source node of this forward_table
        self.latencyOf = {} 
        self.isUpdated = False

    # Return a string
    def __str__(self):
        return "Rewrite this function to define your node dump printout"

    # Fill in this function
    def link_has_been_updated(self, neighbor, latency):
        # latency = -1 if delete a link
        # call Send_To_Neighbors(message) :message identified by user.
        neighbor = str(neighbor)
        if latency == -1 :
            # hasn't been deleted for some reason(not sure for what reason)
            if neighbor in self.latencyOf :
                # remove neighbor from self.neighbors
                self.latencyOf.pop(neighbor) 
                self.neighbors.remove(neighbor)
        # new latency updated
        else :
            # if new neighbor, add to 1)neighbors 2)latencyOf(we choose this one) even just change.
            
            # self.neighbors.append(neighbor);
            if neighbor not in self.latencyOf:
                self.forward_table[str(self.id)][neighbor] = [latency, [neighbor]]
                self.forward_table[neighbor] = {
                    neighbor: [0, []], 
                    str(self.id): [latency, [str(self.id)]], 
                    'SEQ': 0
                }
                self.neighbors.append(neighbor)
                self.isUpdated = True
            self.latencyOf[neighbor] = latency
            
        updated = self.update_forward_table()
        if updated :
            # package a message into json format, remember, forward_table['-1'] = curNode.id
            self.seq = self.seq + 1
            self.forward_table[str(self.id)]['SEQ'] = self.seq
            message = json.dumps(self.forward_table[str(self.id)])
            MSG_HEAD = '"ID": ' + str(self.id) + ', '
            message = message[0] + MSG_HEAD + message[1:]
            # broadcast to neighbors
            self.send_to_neighbors(message)
            self.isUpdated = False




    # Fill in this function
    def process_incoming_routing_message(self, m):
        message = json.loads(m)
        src_node = message['ID']
        src_seq = message['SEQ']
        # seq number problem
        if ( src_seq > self.forward_table[str(src_node)]['SEQ'] ):
            message.pop('ID')
            self.forward_table[str(src_node)] = message
            # pre-process for potential new node for my router
            self.add_new_node(message, src_node)
            updated = self.update_forward_table()
            if updated:
                # package a message into json format, remember, forward_table['-1'] = curNode.id
                self.seq = self.seq + 1
                self.forward_table[str(self.id)]['SEQ'] = self.seq
                message = json.dumps(self.forward_table[str(self.id)])
                MSG_HEAD = '"ID": ' + str(self.id) + ', '
                message = message[0] + MSG_HEAD + message[1:]
                # broadcast to neighbors
                self.send_to_neighbors(message)
                self.isUpdated = False

    # Return a neighbor, -1 if no path to destination
    def get_next_hop(self, destination):
        str_dest = str(destination)

        ret = self.forward_table[str(self.id)][str_dest][PATH][0]
        ret = int(ret)
        return ret
        # return -1

    def update_forward_table(self):
        # 1) update all thing: updated path -> check no loop
        # 2) sequence number
        for node in self.forward_table[str(self.id)].keys():
            if node != 'SEQ' and node != str(self.id):
                # print ("========= BEFORE OPERATING =====")
                # print ("self.forward_table[str(self.id)] = %s" % (self.forward_table[str(self.id)]))
                # print ("==============")

                min_cost = INT_MAX
                # self.forward_table[str(self.id)][node][MIN_DIST]
                min_next_node = ''
                # self.forward_table[str(self.id)][node][PATH][0]

                # for one node, calculate the current min cost, then compare with original next_node and min_cost
                for neighbor in self.latencyOf.keys():
                    # if this neighbor can reach to that node
                    if node in self.forward_table[neighbor].keys():
                        cost = self.latencyOf[neighbor] + self.forward_table[neighbor][node][MIN_DIST]
                        if cost < min_cost:
                            # self.isUpdated = True
                            min_cost = cost
                            min_next_node = neighbor
                
                # print ("==============")
                # print ("curNode = " + str(self.id))
                # print ("node = " + node)
                # print ("min_next_node = " + min_next_node)
                # print ("self.forward_table[str(self.id)] = %s" % (self.forward_table[str(self.id)]))
                # print ("self.forward_table[str(self.id)][node] = %s" % (self.forward_table[str(self.id)][node]))
                # print ("self.forward_table[str(self.id)][node][PATH] = %s" % (self.forward_table[str(self.id)][node][PATH]))
                # print ("==============")
                
                # if haven't updated and there is one update in cost or path.
                if not self.isUpdated and (min_next_node != self.forward_table[str(self.id)][node][PATH][0] or min_cost != self.forward_table[str(self.id)][node][MIN_DIST]) :
                    self.isUpdated = True
                ############### after delete, we need to process this problem
                if self.isUpdated:
                    # if min_cost == self.forward_table[str(self.id)][node][MIN_DIST]: 
                    self.forward_table[str(self.id)][node][MIN_DIST] = min_cost
                    # if min_next_node != self.forward_table[str(self.id)][node][PATH][0]: 
                    deepcopyPath = deepcopy(self.forward_table[min_next_node][node][PATH])
                    deepcopyPath.insert(0, min_next_node)
                    self.forward_table[str(self.id)][node][PATH] = deepcopyPath
                # print ("==============")
                # print ("curNode = " + str(self.id))
                # print ("node = " + node)
                # print ("min_next_node = " + min_next_node)
                # print ("self.forward_table[str(self.id)] = %s" % (self.forward_table[str(self.id)]))
                # print ("self.forward_table[str(self.id)][node] = %s" % (self.forward_table[str(self.id)][node]))
                # print ("self.forward_table[str(self.id)][node][PATH] = %s" % (self.forward_table[str(self.id)][node][PATH]))
                # print ("==============")
        return self.isUpdated

    def add_new_node(self, message, src_node):
        for node in message.keys():
            if node != 'SEQ' and node not in self.forward_table[str(self.id)]:
                dist = message[node][MIN_DIST]
                path = message[node][PATH]
                deepcopyPath = deepcopy(path)
                deepcopyPath.insert(0, str(src_node))
                self.forward_table[str(self.id)][node] = [dist, deepcopyPath]

