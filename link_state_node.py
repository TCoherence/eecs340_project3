from simulator.node import Node
import json
from copy import deepcopy

INT_MAX = 2**20 - 1

# FORWARD TABLE FORMAT
COST = 0
PATH = 1

# LINK TABLE FORMAT
LATENCY = 0
SEQ = 1

# MESSAGE FORMAT = [M_SRC, M_DEST, M_LATENCY, M_SEQ]
M_SRC = 0
M_DEST = 1
M_LATENCY = 2
M_SEQ = 3

class Link_State_Node(Node):
    def __init__(self, id):
        super().__init__(id)
        # self.id = id
        # self.neighbors = []
        self.forward_table = {
            str(self.id): [0, []]
        } # {nodeID:[COST, [PATH]]}
        self.link_table = {
            str(self.id):{

            }
        }    # {nodeID:{nodeID:[latency, SEQ]}}

    # Return a string
    def __str__(self):
        return "Rewrite this function to define your node dump printout"

    # Fill in this function
    def link_has_been_updated(self, neighbor, latency):
        # latency = -1 if delete a link
        neighbor = str(neighbor)
        seq = 0
        if latency == -1:
            seq = self.link_table[str(self.id)][neighbor][SEQ] + 1
            # update link table
            latency = INT_MAX
            self.link_table[str(self.id)][neighbor] = [INT_MAX, seq]
            self.link_table[neighbor][str(self.id)] = [INT_MAX, seq]
            self.neighbors.remove(int(neighbor))
        else :
            # add new node to be a neighbor
            if neighbor not in self.link_table[str(self.id)].keys():
                seq = 0
                self.link_table[str(self.id)][neighbor] = [latency, seq]
                self.link_table[neighbor] = {str(self.id): [latency, seq]}
                self.neighbors.append(int(neighbor))
                for src_node in self.link_table.keys():
                    for dest_node in self.link_table[src_node].keys():
                        m_latency = self.link_table[src_node][dest_node][LATENCY]
                        m_seq = self.link_table[src_node][dest_node][SEQ]
                        message = [int(src_node), int(dest_node), m_latency, m_seq]
                        self.send_to_neighbor(int(neighbor), json.dumps(message))
                        # print ("+++++++++++++++++++++++++++++++++")
                        # print ("messge = " + str(message))
                        # print ("+++++++++++++++++++++++++++++++++")
            # old neighbor
            else :
                seq =  self.link_table[str(self.id)][neighbor][SEQ] + 1
                self.link_table[str(self.id)][neighbor] = [latency, seq]
                self.link_table[neighbor][str(self.id)] = [latency, seq]
        # udpate forward table
        # self.update_forward_table()
        message = [self.id, int(neighbor), latency, seq]
        self.send_to_neighbors(json.dumps(message))


    # Fill in this function
    def process_incoming_routing_message(self, m): 
        msg = json.loads(m)
        src_node = str(msg[M_SRC])
        dest_node = str(msg[M_DEST])
        latency = int(msg[M_LATENCY]) # can be INT_MAX or valid link value
        seq = int(msg[M_SEQ])

        update = True
        # if no edge exit in my link table
        if self.link_table.get(src_node) == None or self.link_table.get(src_node).get(dest_node) == None:
            self.add_edge(src_node, dest_node, latency, seq)
            self.add_edge(dest_node, src_node, latency, seq)
        else :
            if seq > self.link_table[src_node][dest_node][SEQ] : 
                self.link_table[src_node][dest_node] = [latency, seq]
                self.link_table[dest_node][src_node] = [latency, seq]
            else:
                update = False
        # print ("----------- Link Table ---------")
        # print ("curNode = " + str(self.id))
        # print(json.dumps(self.link_table, sort_keys=True, indent=2))
        # print ("--------------------------------")
        
        if update:
            self.send_to_neighbors(m)
        # if seq > self.link_table[src_node][dest_node][SEQ]

    def add_edge(self, src_node, dest_node, latency, seq):
        if self.link_table.get(src_node) != None:
            self.link_table[src_node][dest_node] = [latency, seq]
        else :
            self.link_table[src_node] = {dest_node: [latency, seq]}

    # Return a neighbor, -1 if no path to destination
    def get_next_hop(self, destination):
        self.update_forward_table()
        ret = self.forward_table[str(destination)][PATH][0]
        ret = int(ret)
        # print ("+++++++++++++++++++++++")
        # print ("ret = " + str(ret))
        # print ("+++++++++++++++++++++++")

        return ret
    
    


    def update_forward_table(self):
        self.Dijkstra()

    def Dijkstra(self):
        # initialization
        N = {str(self.id)}
        ALLNODES = set(self.link_table.keys())
        self.forward_table = {str(self.id): [0, []]}
        for node in self.link_table[str(self.id)]:
            cost = self.link_table[str(self.id)][node][LATENCY]
            self.forward_table[node] = [cost, [node]]

        # Real caozuo
        while N < ALLNODES:
            # find the min_cost node and add it to N
            min_cost = INT_MAX
            min_cost_node = ''
            for node in self.forward_table.keys() :
                if node not in N:
                    cost = self.forward_table[node][COST]
                    if min_cost >= cost:
                        min_cost = cost
                        min_cost_node = node
            if min_cost_node == '':
                break
            N.add(min_cost_node)

            ##### Something happens ####
            for neighbor in self.link_table[min_cost_node].keys():
                # if i have seen this neighbor 
                if self.forward_table.get(neighbor) != None :
                    # my min_cost_node provide a smaller cost
                    if neighbor not in N and self.forward_table[neighbor][COST] > self.forward_table[min_cost_node][COST] + self.link_table[min_cost_node][neighbor][LATENCY]:
                        path = deepcopy(self.forward_table[min_cost_node][PATH])
                        path.append(neighbor)
                        self.forward_table[neighbor] = [self.forward_table[min_cost_node][COST] + self.link_table[min_cost_node][neighbor][LATENCY], path]  
                # never seen
                else:
                    path = deepcopy(self.forward_table[min_cost_node][PATH])
                    path.append(neighbor)
                    self.forward_table[neighbor] = [self.forward_table[min_cost_node][COST] + self.link_table[min_cost_node][neighbor][LATENCY], path] 
                    # self.forward_table[neighbor] = [self.forward_table[min_cost_node] + self.link_table[min_cost_node][neighbor][LATENCY], [min_cost_node]]
            # print ("=======================")
            # print ("N = " + str(N))
            # print ("ALLNODES = " + str(ALLNODES))
            # print ("curNode = " + str(self.id))
            # print(json.dumps(self.forward_table, sort_keys=True, indent=4))
            # print ("=======================")

            




